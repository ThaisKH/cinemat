---
title: "Detective Conan: The Fist of Blue Sapphire"
title_link: detective_conan
trailer: "https://www.youtube.com/embed/3rzJtw5EtTw"
vote_average: 6.5
popularity: 81.22
poster_path: "Conan.jpg"
original_language: ja
original_title: "名探偵コナン 紺青の拳（フィスト）"
genre:
- "Animación"
- "Acción"
- "Aventura"
backdrop_path: "/wf6VDSi4aFCZfuD8sX8JAKLfJ5m.jpg"
overview: "Conan Edogawa viaja a Singapur, concretamente a la famosa Marina Bay Sands,
  escenario en el que se ha producido un cruento asesinato. La investigación de
  Conan parece girar en torno a una gema legendaria conocida como 'el Zafiro Azul',
  que lleva sumergido en el océano desde finales del siglo XIX. Makoto Kyôgoku ayudará
  al joven detective a detener a Kaitō Kid, un maestro de karate que quiere robar
  la tan preciada joya."
release_date: "2019-04-12"
duration: "109 min"
country: "Japón"
year: 2019
directors:
- "Tomoka Nagaoka"
actors:
- "Kappei Yamaguchi"
- "Minami Takayama"
- "Wakana Yamazaki"
- "Rikiya Koyama"
---

{% include pelis.html %}

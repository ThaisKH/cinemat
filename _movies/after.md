---
title: "After: Aquí empieza todo"
title_link: after
trailer: "https://www.youtube.com/embed/5ZXzJJzVY-M"
vote_average: 6
popularity: 116.477
poster_path: "After.jpg"
original_language: en
original_title: "After"
genre:
- "Drama"
- "Romance"
- "Suspense"
backdrop_path: "/997ToEZvF2Obp9zNZbY5ELVnmrW.jpg"
overview: "La joven Tessa Young cursa su primer año en la universidad. Acostumbrada
  a una vida estable y ordenada, su mundo cambia cuando conoce a Hardin Scott, un
  misterioso joven de oscuro pasado. Desde el primer momento se odian, porque pertenecen
  a dos mundos distintos y son completamente opuestos. Sin embargo, estos dos polos
  opuestos pronto se unirán y nada volverá a ser igual. Tessa y Hardin deberán enfrentarse
  a difíciles pruebas para estar juntos. La inocencia, el despertar a la vida, el
  descubrimiento sexual y las huellas de un amor tan poderoso como la fuerza del
  destino."
release_date: "2019-04-11"
duration: "106 min"
country: "Estados Unidos"
year: 2019
directors:
- "Jenny Gage"
actors:
- "Hero Fiennes Tiffin"
- "Jennifer Beals"
- "Josephine Langford"
- "Peter Gallagher"
- "Selma Blair"
- "Shane Paul Mcghie"
---

{% include pelis.html %}

---
title: "Pokémon Detective Pikachu"
title_link: detective_pikachu
trailer: "https://www.youtube.com/embed/IN1kuCo_SIc"
vote_average: 7
popularity: 228.029
poster_path: "DetectivePikachu.jpg"
original_language: en
original_title: "Pokémon Detective Pikachu"
genre:
- "Misterio"
- "Acción"
- "Família"
- "Animación"
backdrop_path: "/nDP33LmQwNsnPv29GQazz59HjJI.jpg"
overview: "Ryme City, una metrópoli futurista en la que los humanos y los Pokémon
  conviven en armonía. Tras la misteriosa desaparición de su padre, toda una leyenda
  en la ciudad, el joven Tim Goodman (Justice Smith) comienza una investigación
  para buscarle y averiguar lo que le ha sucedido. En esta misión le ayudará el
  adorable súper-detective Pikachu, un inteligente Pokémon que habla, aunque curiosamente
  el chico es el único que puede entenderle. Ambos unirán sus fuerzas y trabajarán
  juntos para resolver este gran misterio, con la ayuda de Lucy (Kathryn Newton),
  una reportera que trabaja en su primera gran historia. Será una aventura en la
  que descubrirán a entrañables personajes del universo Pokémon, además de una trama
  espeluznante que podría amenazar la convivencia pacífica de todo este universo."
release_date: "2019-05-03"
duration: "104 min"
country: "Estados Unidos"
year: 2019
directors:
- "Rob Letterman"
actors:
- "Bill Nighy"
- "Justice Smith"
- "Kathryn Newton"
- "Ken Watanabe"
- "Ryan Reynolds"
- "Suki Waterhouse"
---

{% include pelis.html %}

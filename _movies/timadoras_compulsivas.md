---
title: "Timadoras compulsivas"
title_link: timadoras_compulsivas
trailer: "https://www.youtube.com/embed/4_x0dKb3G3M"
vote_average: 6.2
popularity: 56.18
poster_path: "TimadorasCompulsivas.jpg"
original_language: en
original_title: "The Hustle"
genre:
- "Comedia"
backdrop_path: "/s6awXOxTKYQLSktiIJfI3969dZH.jpg"
overview: "Dos artistas del engaño, una de clase alta y la otra de los barrios bajos,
  deciden unirse para timar a los hombres... Remake de la comedia de 1988 'Dirty
  Rotten Scoundrels'."
release_date: "2019-05-09"
duration: "94 min"
country: "Estados Unidos"
year: 2019
directors:
- "Chris Addison"
actors:
- "Alex Sharp"
- "Anne Hathaway"
- "Emma Davies"
- "Ingrid Oliver"
- "Rebel Wilson"
- "Tim Blake Nelson"
---

{% include pelis.html %}

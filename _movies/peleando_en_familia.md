---
title: "Peleando en familia"
title_link: peleando_en_familia
trailer: "https://www.youtube.com/embed/iMpIih1fJys"
vote_average: 6.5
popularity: 73.462
poster_path: "PeleandoFamilia.jpg"
original_language: en
original_title: "Fighting with My Family"
genre:
- "Comedia"
- "Drama"
backdrop_path: "/mYKP6qcDUimVMAHQWLOuDHjO19S.jpg"
overview: "Un antiguo luchador de lucha libre y su familia se ganan la vida actuando
  a lo largo del país, pero sus hijos sueñan con poder vivir del Wrestling."
release_date: "2019-02-14"
duration: "108 min"
country: "Reino Unido"
year: 2019
directors:
- "Stephen Merchant"
actors:
- "Florence Pugh"
- "Vince Vaughn"
- "Dwayne Johnson"
- "Lena Headey"
- "Nick Frost"
- "Jack Lowden"
---

{% include pelis.html %}

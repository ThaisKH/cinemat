---
title: "Nosotros"
title_link: nosotros
trailer: "https://www.youtube.com/embed/nzCyY_fTowA"
vote_average: 7.2
popularity: 57.89
poster_path: "Nosotros.jpg"
original_language: en
original_title: Us
genre:
- "Suspense"
- "Terror"
backdrop_path: "/jNUCddkM1fjYcFIcEwFjc7s2H4V.jpg"
overview: "Una familia se dispone a ir a la playa y disfrutar del sol. Jason, el
  más joven de la misma, desaparece. Cuando sus padres le encuentran, el niño parece
  estar desorientado. Esa noche, una nueva familia trata de aterrorizarles y lo
  consiguen cuando se dan cuenta de que son físicamente y emocionalmente iguales.
  Ahora, la única salida es matar a la familia impostora antes de que esta acabe
  con ellos."
release_date: "2019-03-14"
duration: "116 min"
country: "Estados Unidos"
year: 2019
directors:
- "Jordan Peele"
actors:
- "Elisabeth Moss"
- "Evan Alex"
- "Lupita Nyongo"
- "Shahadi Wright Joseph"
- "Tim Heidecker"
- "Winston Duke"
---

{% include pelis.html %}

---
title: "La Llorona"
title_link: la_llorona
trailer: "https://www.youtube.com/embed/JoTbiH3Wppo"
vote_average: 5.5
popularity: 68.304
poster_path: "Llorona.jpg"
original_language: en
original_title: "The Curse of La Llorona"
genre:
- "Terror"
- "Misterio"
- "Thriller"
backdrop_path: "/u2vGggeMPAkhEtD7bYGfeThsQiM.jpg"
overview: "La Llorona es una aparición tenebrosa, atrapada entre el Cielo y el Infierno,
  con un destino terrible sellado por su propia mano. La mera mención de su nombre
  ha causado terror en todo el mundo durante generaciones. En vida, ahogó a sus
  hijos en una rabia de celos, arrojándose en el río tras ver como mató a sus hijos
  violentamente. Ahora sus lágrimas son eternas, letales, y aquellos que escuchan
  su llamada de muerte en la noche están condenados. Se arrastra en las sombras
  y ataca a los niños, desesperada por reemplazar a los suyos. A medida que los
  siglos han pasado, su deseo se ha vuelto más voraz y sus métodos más terroríficos."
release_date: "2019-04-17"
duration: "93 min"
country: "Estados Unidos"
year: 2019
directors:
- "Michael Chaves"
actors:
- "Linda Cardellini"
- "Patricia Velasquez"
- "Sean Patrick Thomas"
- "Tony Amendola"
- "Raymond Cruz"
- "Marisol Ramirez"
---

{% include pelis.html %}

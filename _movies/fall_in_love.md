---
title: "Fall in Love at First Kiss"
title_link: fall_in_love
trailer: "https://www.youtube.com/embed/YbDOMMIoiA4"
vote_average: 6
popularity: 113.57
poster_path: "FallInLove.jpg"
original_language: zh
original_title: "一吻定情"
genre:
- "Comedia"
- "Romance"
backdrop_path: "/yY7kr7Rgvo8W0Kr3U0bMV8Zq1qM.jpg"
overview: "Una adolescente se enamora de un estudiante de un curso superior el primer
  día de instituto, pero no se lo dice. Cuando un huracán destruye su casa, ella
  y su familia se ven obligados a mudarse a la casa del mejor amigo de la universidad
  de su padre, que es también el padre del chico que le gusta."
release_date: "2019-02-14"
duration: "123 min"
country: "China"
year: 2019
directors:
- "Yu Shan Chen"
actors:
- "Darren Wang"
- "Lin Yuan"
---

{% include pelis.html %}
